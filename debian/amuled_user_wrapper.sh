#!/bin/sh

# This script gets called by systemd, and its purpose is to honour the
# AMULED_USER in /etc/default/amule-daemon when starting amuled via
# /usr/share/amule/amuled_home_wrapper.sh.

. /etc/default/amule-daemon

if [ -z "$AMULED_USER" ]; then
	echo "Not starting aMule daemon, AMULED_USER not set in /etc/default/amule-daemon."
	exit 1
fi

runuser "$AMULED_USER" /usr/share/amule/amuled_home_wrapper.sh
