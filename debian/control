Source: amule
Section: net
Priority: optional
Maintainer: Sandro Tosi <morph@debian.org>
Build-Depends: autoconf,
               automake,
               autopoint,
               autotools-dev,
               debhelper-compat (= 13),
               libboost-dev,
               libboost-system-dev,
               libcrypto++-dev,
               libgd-dev,
               libgeoip-dev,
               libglib2.0-dev,
               libpng-dev,
               libreadline-dev,
               libupnp-dev,
               libwxgtk3.2-dev,
               wx3.2-i18n,
               zlib1g-dev,
Standards-Version: 4.6.0
Homepage: https://www.amule.org
Vcs-Git: https://salsa.debian.org/debian/amule.git
Vcs-Browser: https://salsa.debian.org/debian/amule

Package: amule
Architecture: any
Depends: amule-common (= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Recommends: amule-utils,
            unzip,
Suggests: amule-utils-gui,
Description: client for the eD2k and Kad networks, like eMule
 aMule is a peer-to-peer file sharing application, designed to connect
 to the eDonkey and Kad networks. It has a wide range of features,
 including many of the original eMule client, like:
 .
  * online signature, source exchange, compressed transfers, secure
    identification, and IP filter support
  * boolean search, which can be local, global, or in the Kad network
  * checks against aggressive clients
  * slot allocation, to decide the number of remote clients
  * systray works well both in GNOME and KDE
  * translations to many languages
 .
 A daemonized version of the application that does not need a graphic
 environment to run is available in the amule-daemon package, and
 various utilities of interest can be found in the amule-utils and
 amule-utils-gui packages, including the ed2k link handler.

Package: amule-common
Architecture: all
Depends: ${misc:Depends},
         ${shlibs:Depends},
Multi-Arch: foreign
Description: common files for the rest of aMule packages
 This package contains localization files, webserver templates and GUI skins
 for aMule. You probably don't want to install this package alone, but amule,
 amule-daemon or amule-utils-gui instead.

Package: amule-utils
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
Recommends: amule-common,
            fonts-dejavu-core,
Suggests: amule-gnome-support,
Breaks: xmule (<= 1.10.0b-1),
Description: utilities for aMule (command-line version)
 This package contains a set of command-line utilities related to aMule,
 the eD2k network client:
 .
  * ed2k: handles ed2k:// links, queueing them into aMule
  * cas: displays the contents of your aMule online signature
  * alcc: computes ed2k:// links for the given input files
  * amulecmd: text-based client to control aMule or the aMule daemon
 .
 Some of these utilities have graphic versions, which can be found in
 the amule-utils-gui package.

Package: amule-utils-gui
Architecture: any
Replaces: astats,
Depends: ${misc:Depends},
         ${shlibs:Depends},
Recommends: amule-common,
Description: graphic utilities for aMule
 This package contains a set of graphic utilities related to aMule, the
 eD2k network client:
 .
  * wxcas: displays the contents of your aMule online signature
  * alc: computes ed2k:// links for the given input files
  * amulegui: graphic client to control aMule or the aMule daemon
 .
 A command-line version of these utilities can be found in the
 amule-utils package.

Package: amule-daemon
Architecture: any
Depends: amule-common (= ${source:Version}),
         lsb-base,
         ${misc:Depends},
         ${shlibs:Depends},
Recommends: amule-utils,
            unzip,
Description: non-graphic version of aMule, a client for the eD2k and Kad networks
 This package contains a daemonized version of aMule, amuled, that does
 not need a graphic environment to run, and can run in the background as
 well. It is normally used to be run in a machine 24/7, since the
 application continues to run if the X11 session closes.
 .
 Included in the package is a webserver that provides an interface to
 control the daemon. Remote connections are supported, and the daemon
 can be configured to start the webserver automatically at startup. It
 is also possible to control amuled via amulecmd, from the amule-utils
 package, and amulegui, available in the amule-utils-gui package.
 .
 See the description of the amule package for a list of features, and
 /usr/share/doc/amule-daemon/README.Debian for some basic usage notes.

Package: amule-gnome-support
Architecture: all
Depends: amule-utils,
         ${misc:Depends},
Recommends: amule | amule-daemon,
Description: ed2k links handling support for GNOME web browsers
 This package contains a schemas file that allows ed2k links handling support
 with any GNOME web browser that use GConf. For example: Firefox, Epiphany,
 Flock, Seamonkey or Galeon.
